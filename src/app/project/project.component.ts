import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { MdDialogRef, MdDialog } from '@angular/material';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { AddTaskDialogComponent } from '../add-task-dialog/add-task-dialog.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  @Input("projectKey") projectKey: string;
  project: any;
  @Input("uid") uid: string;
  @Input("userData") userData: { firstName: string, lastName: string };

  projectOwnerTaskCount: number = 0;
  users: Array<{key: string, taskCount: number, user: any}> = [];
  openTasks: Array<{key: string, task: any}> = [];
  myTasks: Array<{key: string, task: any}> = [];
  completedTasks: Array<{key: string, task: any}> = [];

  numOfCols: string = '2';

  constructor(private af: AngularFire, private dialog: MdDialog, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.numOfCols = Math.floor(window.innerWidth / 300).toString();  
  }

  ngAfterViewInit(){
    this.init();
  }

  init(){
    this.af.database.object("/projects/" + this.projectKey).subscribe((project) => {
      this.refresh(project);
    });
  }

  refresh(project){
      this.project = project;
      this.openTasks = [];
      this.myTasks = [];
      this.completedTasks = [];
      this.users = [];

      if(!this.project)
      return;
      
      if(!this.project.tasks)
      return;
      
      for(let taskId in this.project.tasks)
      {
        if(this.project.tasks[taskId].isComplete === true){
          this.completedTasks.push({key: taskId, task: this.project.tasks[taskId]});
        }
        else{
          this.openTasks.push({ key: taskId, task: this.project.tasks[taskId]});

          if(this.project.tasks[taskId].owners){
            for(let i in this.project.tasks[taskId].owners){
              if(i == this.uid){
                this.myTasks.push({key: taskId, task: this.project.tasks[taskId]});
                break;
              }
            }
          }
        }
      }

      for(let i in this.project.users){
          let taskCount = 0;
          for(let taskId in this.project.tasks)
          {
              if(this.project.tasks[taskId].owners){
                for(let u in this.project.tasks[taskId].owners){
                  if(u === i && !this.project.tasks[taskId].isComplete){
                    taskCount++;
                  }
                }
              }
          }
          this.users.push({ key: i, taskCount: taskCount, user: this.project.users[i] });
      }
      
      // get task count for project owners
      let taskCount = 0;
      for(let taskId in this.project.tasks)
      {
          if(this.project.tasks[taskId].owners){
            for(let u in this.project.tasks[taskId].owners){
              if(u === this.project.owner.id && !this.project.tasks[taskId].isComplete){
                taskCount++;
              }
            }
          }
      }
      this.projectOwnerTaskCount = taskCount;

      this.cdr.detectChanges();
  }

  onResize(){
    this.numOfCols = Math.floor(window.innerWidth / 300).toString();
    this.cdr.detectChanges();
  }

  openAddTaskDialog(){
    let dialogRef = this.dialog.open(AddTaskDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      let instance = dialogRef.componentInstance;
      if(instance.didCreate){
        this.addTask(instance.taskName, instance.taskDesc);
      }
    });
  }

  addTask(name: string, desc: string){
    let tasks = this.af.database.list("/projects/" + this.projectKey + "/tasks");
    tasks.push({ name: name, description: desc, isComplete: false, creator: this.userData.firstName + " " + this.userData.lastName});
  }

  openUserDialog(userKey: string){
    this.af.database.object("/users/" + userKey).subscribe((user) => {

    });
  }
}

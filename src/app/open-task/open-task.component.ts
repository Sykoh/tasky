import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { MdDialogRef, MdDialog } from '@angular/material';
import { TaskInfoDialogComponent } from '../task-info-dialog/task-info-dialog.component';
import { CompleteTaskDialogComponent } from '../complete-task-dialog/complete-task-dialog.component';

@Component({
  selector: 'app-open-task',
  templateUrl: './open-task.component.html',
  styleUrls: ['./open-task.component.css']
})
export class OpenTaskComponent implements OnInit {

  @Input("task") task: { key: string, task: any };
  @Input("uid") uid: string;
  @Input("userData") userData: { firstName: string, lastName: string};
  @Input("projectKey") projectKey: string;

  @Output("refresh") refreshEvent: EventEmitter<any> = new EventEmitter<any>();

  isAssigned: boolean = false;
  ownersString: string = "";

  constructor(private af: AngularFire, private dialog: MdDialog) { }

  ngOnInit() {
    if(this.task.task.owners){
      for(let i in this.task.task.owners){
        if(i === this.uid){
          this.isAssigned = true;
        }
        this.ownersString += "[" + this.task.task.owners[i].name + "] ";
      }
    }
  }

  assign(){
    this.isAssigned = true;
    console.log(this.uid);
    let owner= this.af.database.object("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/owners/" + this.uid);
    owner.set({name: this.userData.firstName + " " + this.userData.lastName});
    this.refreshEvent.emit();
  }

  unassign(){
    this.isAssigned = false;
    let ownersList = this.af.database.list("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/owners");
    ownersList.remove(this.uid);
    this.refreshEvent.emit();
  }

  moreInfo(){
    let dialogRef = this.dialog.open(TaskInfoDialogComponent, { width: '640px', height: '480px' });
    dialogRef.componentInstance.task = this.task;
    dialogRef.componentInstance.ownersString = this.ownersString;
    dialogRef.componentInstance.isAssigned = this.isAssigned;
    dialogRef.componentInstance.projectKey = this.projectKey;
    dialogRef.componentInstance.parentComponent = this;
    dialogRef.componentInstance.userData = this.userData;
    dialogRef.afterClosed().subscribe((data) => {

    });
    
  }

  refresh(){
    this.refreshEvent.emit();
  }

  complete(){
    let dialogRef = this.dialog.open(CompleteTaskDialogComponent);
    dialogRef.afterClosed().subscribe((data) => {
      if(dialogRef.componentInstance.didComplete === true){
        if(dialogRef.componentInstance.comment){
          if(dialogRef.componentInstance.comment != ""){
            let taskCommentRef = this.af.database.list("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/comments");
            taskCommentRef.push({ name: this.userData.firstName + " " + this.userData.lastName,
            message: dialogRef.componentInstance.comment,
            date: new Date().toUTCString()});
          }
        }

        let taskCompleteRef = this.af.database.object("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/isComplete");
        taskCompleteRef.set(true).then(() => {
          this.refresh();
        });
      }
    });
  }

}

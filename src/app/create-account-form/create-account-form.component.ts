import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-create-account-form',
  templateUrl: './create-account-form.component.html',
  styleUrls: ['./create-account-form.component.css']
})
export class CreateAccountFormComponent implements OnInit {

  message: string = "";
  firstName: string = "";
  lastName: string = "";
  email: string = "";
  password: string = "";

  status: string = "create"

  @Output() onCreate: EventEmitter<{ email: string, password: string }> = new EventEmitter<{ email: string, password: string }>();
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  constructor(private af: AngularFire) { }

  ngOnInit() {
  }

  createAccount(){
    this.status = "progress";
    this.af.auth.createUser({ email: this.email, password: this.password }).then((state) => {
      let user = this.af.database.object("/users/" + state.uid);
      user.set({ firstName: this.firstName, lastName: this.lastName });
      this.status = "confirm";
      this.message = "A confirmation email has been sent. Please verify your email.";
      state.auth.sendEmailVerification().then((sucess) => {
        this.onCreate.emit({ email: this.email, password: this.password });
      });
    }).catch((err) => {
      this.status = "create";
      this.message = err.message;
    });
  }

  cancel(){
    this.status = "create";
    this.onCancel.emit(false);
  }

}

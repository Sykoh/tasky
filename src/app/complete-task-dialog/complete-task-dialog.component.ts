import { Component, OnInit } from '@angular/core';
import {MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-complete-task-dialog',
  templateUrl: './complete-task-dialog.component.html',
  styleUrls: ['./complete-task-dialog.component.css']
})
export class CompleteTaskDialogComponent implements OnInit {

  didComplete: boolean = false;
  comment: string;

  constructor(private dialogRef: MdDialogRef<CompleteTaskDialogComponent>) { }

  ngOnInit() {
  }

  complete(){
    this.didComplete = true;
    this.dialogRef.close();
  }

  cancel(){
    this.didComplete = false;
    this.dialogRef.close();
  }
}

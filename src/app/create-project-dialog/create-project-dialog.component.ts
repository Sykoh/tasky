import { Component, OnInit, Input } from '@angular/core';
import {MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-create-project-dialog',
  templateUrl: './create-project-dialog.component.html',
  styleUrls: ['./create-project-dialog.component.css']
})
export class CreateProjectDialogComponent implements OnInit {

  didCreate: boolean;
  projectName: string;

  constructor(private dialogRef: MdDialogRef<CreateProjectDialogComponent>) { }

  ngOnInit() {
  }

  create(){
    this.didCreate = true;
    this.dialogRef.close();
  }

  cancel(){
    this.didCreate = false;
    this.dialogRef.close();
  }
}

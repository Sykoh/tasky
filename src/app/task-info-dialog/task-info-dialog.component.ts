import { Component, OnInit, EventEmitter } from '@angular/core';
import { MdDialogRef, MdDialog } from '@angular/material';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { OpenTaskComponent } from '../open-task/open-task.component';

@Component({
  selector: 'app-task-info-dialog',
  templateUrl: './task-info-dialog.component.html',
  styleUrls: ['./task-info-dialog.component.css']
})
export class TaskInfoDialogComponent implements OnInit {

  task: { key: string, task: any };
  isAssigned: boolean = false;
  ownersString: string;
  commentText: string;
  projectKey: string;
  parentComponent: OpenTaskComponent;
  userData: { firstName: string, lastName: string };
  comments: Array<{ key: string, comment: { name: string, message: string, date: string } }> = [];

  constructor(private dialogRef: MdDialogRef<TaskInfoDialogComponent>, private af: AngularFire) { }

  ngOnInit() {
    this.sortComments(this.task.task.comments);

    this.af.database.object("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/comments").subscribe((comments) => {
       this.sortComments(comments);
    });
  }

  sortComments(comments: any){
    this.comments = [];
    if(!comments)
      return;

    for(let i in comments){
      if(comments[i]){
        if(comments[i].date){
          this.comments.push({ key: i, comment: comments[i] });
        }
      } 
    }

    if(this.comments.length > 0)
      this.comments.sort((a, b) => {return new Date(b.comment.date).getTime()-new Date(a.comment.date).getTime();});

      this.comments.forEach((comment) => { comment.comment.date = new Date(comment.comment.date).toLocaleDateString() + " " + new Date(comment.comment.date).toLocaleTimeString() });
  }

  submitComment(){
    let commentsRef = this.af.database.list("/projects/" + this.projectKey + "/tasks/" + this.task.key + "/comments");
    if(this.commentText && this.commentText != ""){
      let date = new Date();
      let obj = { name: this.userData.firstName + " " + this.userData.lastName, message: this.commentText, date: date.toUTCString()};
      commentsRef.push(obj).then((comment) => {
        this.commentText = "";
        this.parentComponent.refresh();
      });
    }
  }

}

import { Component, OnInit, Output, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
declare var electron: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input("title") title: string;
  @Output("onMenuClick") onMenuClick: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild("toolbar") tbar: any;

  constructor() { 
  }

  ngOnInit() {
  }

  getBarHeight(){
    return this.tbar.elementRef.nativeElement.clientHeight;
  }

  openMenu(){
    this.onMenuClick.emit(true);
  }

  minimize(){
    let remote = electron.remote;
    remote.getCurrentWindow().minimize();
  }

  maximized: boolean = false;

  maximize(){
    let remote = electron.remote;
    if(remote.getCurrentWindow().isMaximized())
      remote.getCurrentWindow().unmaximize();
    else
      remote.getCurrentWindow().maximize();
  }

  close(){
    let remote = electron.remote;
    remote.getCurrentWindow().close();
  }

}

import { Component, OnInit } from '@angular/core';
import {MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-task-dialog',
  templateUrl: './add-task-dialog.component.html',
  styleUrls: ['./add-task-dialog.component.css']
})
export class AddTaskDialogComponent implements OnInit {

  didCreate: boolean;
  taskName: string;
  taskDesc: string;

  constructor(private dialogRef: MdDialogRef<AddTaskDialogComponent>) { }

  ngOnInit() {
  }

  create(){
    this.didCreate = true;
    this.dialogRef.close();
  }

  cancel(){
    this.didCreate = false;
    this.dialogRef.close();
  }
}

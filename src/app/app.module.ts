import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Type } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MaterialModule } from '@angular/material';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { NavbarComponent } from './navbar/navbar.component';
import { CreateProjectDialogComponent } from './create-project-dialog/create-project-dialog.component';
import { ProjectComponent } from './project/project.component';
import { AddTaskDialogComponent } from './add-task-dialog/add-task-dialog.component';
import { CreateAccountFormComponent } from './create-account-form/create-account-form.component';
import { OpenTaskComponent } from './open-task/open-task.component';
import { CompletedTaskComponent } from './completed-task/completed-task.component';
import { TaskInfoDialogComponent } from './task-info-dialog/task-info-dialog.component';
import { CompleteTaskDialogComponent } from './complete-task-dialog/complete-task-dialog.component';
import { AddProjectDialogComponent } from './add-project-dialog/add-project-dialog.component';

const myFirebaseConfig = {
    apiKey: "AIzaSyDjTen9XtridYVfeMCpSPT8S4RiT3M08Gc",
    authDomain: "tasky-48b66.firebaseapp.com",
    databaseURL: "https://tasky-48b66.firebaseio.com",
    storageBucket: "tasky-48b66.appspot.com",
    messagingSenderId: "357993464866"
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CreateProjectDialogComponent,
    ProjectComponent,
    AddTaskDialogComponent,
    CreateAccountFormComponent,
    OpenTaskComponent,
    CompletedTaskComponent,
    TaskInfoDialogComponent,
    CompleteTaskDialogComponent,
    AddProjectDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDjTen9XtridYVfeMCpSPT8S4RiT3M08Gc",
      authDomain: "tasky-48b66.firebaseapp.com",
      databaseURL: "https://tasky-48b66.firebaseio.com",
      storageBucket: "tasky-48b66.appspot.com",
      messagingSenderId: "357993464866"
    }, myFirebaseAuthConfig),
    MaterialModule.forRoot()
  ],
  entryComponents: [
    CreateProjectDialogComponent,
    AddTaskDialogComponent,
    TaskInfoDialogComponent,
    CompleteTaskDialogComponent,
    AddProjectDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

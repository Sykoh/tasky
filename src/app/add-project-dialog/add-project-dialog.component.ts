import { Component, OnInit } from '@angular/core';
import {MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-project-dialog',
  templateUrl: './add-project-dialog.component.html',
  styleUrls: ['./add-project-dialog.component.css']
})
export class AddProjectDialogComponent implements OnInit {

  didAdd: boolean = false;
  projectKey: string;

  constructor(private dialogRef: MdDialogRef<AddProjectDialogComponent>) { }

  ngOnInit() {
  }

  add(){
    this.didAdd = true;
    this.dialogRef.close();
  }

  cancel(){
    this.didAdd = false;
    this.dialogRef.close();
  }

}

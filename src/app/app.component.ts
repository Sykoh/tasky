import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { MdSidenav, MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';
import { NavbarComponent} from './navbar/navbar.component';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { CreateProjectDialogComponent } from './create-project-dialog/create-project-dialog.component';
import { AddProjectDialogComponent } from './add-project-dialog/add-project-dialog.component';
import { ProjectComponent } from './project/project.component';
import 'rxjs/add/operator/take';

declare var electron: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  @ViewChild(MdSidenav) sideNav: MdSidenav;
  @ViewChild(NavbarComponent) navBar: NavbarComponent;
  @ViewChild(ProjectComponent) projectComponent: ProjectComponent;

  selectedProject: any = null;
  sidebarWidth: string = '30%';

  barHeight: string = '60px';

  email: string;
  password: string;
  loginMessage = "";
  uid: string = "";
  userData: { firstName: string, lastName: string } = {
    firstName: "",
    lastName: ""
  };

  resendVerificationEmail: boolean = false;

  myProjects: Array<any> = [];
  otherProjects: Array<any> = [];

  userObs: FirebaseObjectObservable<any>;
  userProjsObs: FirebaseListObservable<any>;

  constructor(private cdr: ChangeDetectorRef, private af: AngularFire, private dialog: MdDialog, public snackBar: MdSnackBar){ 
    this.init();
  }

  init(){
        // Load user
    
    this.af.auth.subscribe((authState) => {
      console.log(authState);
      if(authState && authState !== null && authState.uid !== null){
        if(!authState.auth.emailVerified){
          this.uid = "verify";
          if(this.resendVerificationEmail){
            authState.auth.sendEmailVerification();
            this.resendVerificationEmail = false;
          }
          authState.auth.reload();
          return;
        }
        this.uid = authState.uid;
        this.userObs = this.af.database.object("/users/" + this.uid);
        this.userObs.subscribe((userData) => {
          this.userData = userData;

          this.userProjsObs = this.af.database.list("/users/" + this.uid + "/projects");
          this.userProjsObs.subscribe((projects) => {
            this.myProjects = [];
            this.otherProjects = [];
            console.log(projects);
            for(let project of projects){
              if(project.ownerKey === this.uid){
                this.myProjects.push(project);
              }
              else{
                this.otherProjects.push(project);
              }
            }
            this.cdr.detectChanges();
          }, (error) => {
            console.log(error);
              this.uid = "null";
          });
        },
        (error) => {
          console.log(error);
        });
      }
      else{
        this.uid = "null";
      }
      
    }, (error) => {

    }, () => {

    });
  }

  ngOnInit(){
    if(window.innerWidth < 768){
      this.sidebarWidth = '50%';
    }
    else{
      this.sidebarWidth = '30%';
    }
  }

  ngAfterViewInit(){
    this.barHeight = this.navBar.getBarHeight() + "px";
    this.cdr.detectChanges();
  }

  onResize(event){
    this.barHeight = this.navBar.getBarHeight() + "px";
    this.cdr.detectChanges();
  }

  openMenu(event: boolean){  
    this.sideNav.open();
  }

  login(){
    if(this.email && this.password){
      this.uid = "progress";
      this.af.auth.login({ email: this.email, password: this.password }).then((user) => {
        
      }).catch((err) => {
        this.loginMessage = err.message;
        this.uid = "null";
      });
    }
    else{
      this.loginMessage = "You must provide an email and password.";
    }
  }

  logout(){
    this.uid = "progress";
    this.selectedProject = null;
    this.af.auth.logout();
  }

  openProjectDialog(){
    let dialogRef = this.dialog.open(CreateProjectDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      let instance = dialogRef.componentInstance;
      if(instance.didCreate){
        this.createProject(instance.projectName);
      }
    });
  }

  createProject(name: string){
    let projects = this.af.database.list("/projects");
    let projectId = projects.push({ name: name, owner: { id: this.uid, name: this.userData.firstName + " " + this.userData.lastName}, tasks: [], users: []}).then((item) => {
      let userProjects = this.af.database.list("/users/" + this.uid + "/projects");
      userProjects.push({name: name, key: item.key, ownerKey: this.uid});
    });
  }

  openAddProjectDialog(){
    let dialogRef = this.dialog.open(AddProjectDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      let instance = dialogRef.componentInstance;
      if(instance.didAdd){
        this.addProject(instance.projectKey);
      }
    });
  }

  addProject(key: string){
    this.af.database.object("/projects/" + key).take(1).subscribe((project) => {
      if(project.$value !== null){
        let uprojs = this.af.database.list("/users/" + this.uid + "/projects");
      
        this.af.database.list("/users/" + this.uid + "/projects").take(1).subscribe(uprojs => {
          let filtered = uprojs.filter((item) => { return (item.key == key); });
          if(filtered.length > 0){
            // Projects already added
            this.snackBar.open("Project has already been added..", "OK", { duration: 2000 });
          }
          else{
            // Project can be added
            let uProjRef = this.af.database.list("/users/" + this.uid + "/projects");
            uProjRef.push({ key: key , name: project.name }).then(() => {
              let projUsersRef = this.af.database.object("/projects/" + key + "/users/" + this.uid);
              projUsersRef.set({ name: this.userData.firstName + " " + this.userData.lastName }).then(() => {
                  this.snackBar.open("Project added sucessfully!", "OK", {duration: 2000});
              });
            });
          }
        });
      }
      else{
        // Project does not exist
        this.snackBar.open("Project does not exist..", "OK", { duration: 2000 });
      }
    });
  }

  selectProject(project: any){
    this.selectedProject = project.key;
    this.cdr.detectChanges();
    this.projectComponent.init();
    this.sideNav.close();
  }

  createAccount(){
    this.uid = "create";
  }

  onCreateAccount(creds: { email: string, password: string }){
    this.uid = "progress";
      this.af.auth.login({ email: creds.email, password: creds.password }).then((user) => {
      }).catch((err) => {
        this.loginMessage = err.message;
        this.uid = "null";
      });
  }

  onCancelCreateAccount(event){
    this.loginMessage = "";
    this.uid = "null";
  }

  resendEmail(){
    this.resendVerificationEmail = true;
    this.init();
  }

  verifyRefresh(){
    this.uid = "progress";
    this.init();
  }
}

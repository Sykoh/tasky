import { TaskyPage } from './app.po';

describe('tasky App', function() {
  let page: TaskyPage;

  beforeEach(() => {
    page = new TaskyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
